<%@page contentType="text/html" pageEncoding="UTF-8"
%><%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" 
%><!DOCTYPE html>
<html lang="ru">
  <head>
    <title>СО Верна - оплата заказа</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="static/css/payment.css?15241426807741" type="text/css"  data-template-style="true"  rel="stylesheet" />
    <link href="static/css/media_payment.css?15096161182428" type="text/css"  data-template-style="true"  rel="stylesheet" />
    <link rel="icon" href="favicon.ico" type="image/x-icon">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js"></script>
    <script type="text/javascript" src="static/js/payment.js" ></script>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.10/css/all.css" integrity="sha384-+d0P83n9kaQMCwj8F4RJB66tzIwOKmrdb46+porD/OvrJ+37WqIM7UoBtwHO6Nlg" crossorigin="anonymous">
  <body class="payment">
    <header>
      <div class="wrapp">
        <div class="header-logo"></div>
        <h1 class="head">Оплата заказа</h1>
      </div>
    </header>
    <section>
      <div class="wrapp">
        <div id="payment_policy_status">
          <c:choose>
            <c:when test="${success}">
              <div class="payment_types">
                <i class="fa fa-check-circle fa-2x fa-fw"></i><span>Оплата заказа прошла успешно</span>
              </div>
              <div id="status_wait">
                <div class="icon-loader"><i class="fas fa-spinner fa-pulse fa-3x fa-fw"></i></div>
                <div id="calc_status">Получение данных полиса...</div>
              </div>
              <div id="policy_error">
                <i class="fa fa-exclamation-circle fa-3x fa-fw"></i>
                <div class="error-text"></div>
              </div>
              <div id="payment_policy_result">
                <h3><i class="fa fa-thumbs-up fa-2x fa-fw"></i><br/><br/>Полис оформлен</h3>
                <div id="policy_data_container">
                  <p><span>Страхователь (ФИО):</span><b id="insurer">-</b></p>
                  <p><span>Email:</span><b id="email">-</b></p>
                  <p><span>Номер полиса:</span><b id="policy_id">-</b></p>
                  <p><span>Дата действия:</span><b id="begin_date">-</b></p>
                  <p><span>Окончание действия:</span><b id="end_date">-</b></p>
                </div>
                <%--div id="policy-add-info">Вся информация о полисе будет отображаться в вашем личном кабинете</div>--%>
              </div>
            </c:when>
            <c:otherwise>
              <div class="payment_types_red">
                <i style="color: red" class="fa fa-ban fa-2x fa-fw"></i><span style="color:red">Неуспешная оплата полиса</span>
              </div>
              <div class="payment">
                <div id="payment_error" class="active">
                  <span class="error-text">Сообщение банка: ${bankActionDescr}</span>
                </div>
              </div>
            </c:otherwise>
          </c:choose>
        </div>
      </div>
    </section>
    <section class="bordered">
      <div class="wrapp">
        <a href="${success ? partner_success_url : partner_failure_url}"><div class="btn">Вернуться на сайт партнера</div></a>
      </div>
    </section>
    <script>
      var calc_id = '${calc_id}';
      var payment_success = ${success};
    </script>
  </body>
</html>