package ru.verna.documentation;

import org.json.JSONException;
import org.json.JSONObject;
import org.json.XML;
import ru.verna.commons.DbUtils;
import ru.verna.commons.Utils;
import ru.verna.commons.log.Log;
import ru.verna.commons.model.ActionParam;
import ru.verna.commons.model.ParamStructure;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Map;
import java.util.TreeMap;

/**
 * Реализация интерфейса получения данных по документации из БД.
 */
class DocumentationServiceImpl implements DocumentationService {

    /**
     * @return список действий в формате XML строки
     */
    @Override
    public String getActionList() {
        String showAll = isShowAllQuery();
        String additionalSql = Utils.isEmpty(showAll) ? "" : (" WHERE " + showAll);
        final String sql = "SELECT id, descr FROM public.actions" + additionalSql + " ORDER BY id";
        StringBuffer buffer = new StringBuffer();
        try (Connection connection = DbUtils.getConnection();
                PreparedStatement statement = connection.prepareStatement(sql);
                ResultSet resultSet = statement.executeQuery()) {
            buffer.append("<data><result>");
            while (resultSet.next()) {
                buffer.append(wrapInXmlTags(resultSet.getString(1), resultSet.getString(2)));
            }
            buffer.append("</result></data>");
        } catch (SQLException e) {
            Log.getLogger("db").error(e.getLocalizedMessage());
        }
        return buffer.toString();
    }

    /**
     * Формирует из переданных значений строку в формате XML ноды.
     *
     * @param actionId имя действия
     * @param actionDescr описание действия
     * @return переданные данные в виде строки в формате XML ноды
     */
    private String wrapInXmlTags(String actionId, String actionDescr) {
        return "<row>"
                + "<id>" + actionId + "</id>"
                + "<descr>" + actionDescr + "</descr>"
                + "</row>";
    }

    /**
     * Формирует строку в формате JSON где ключами являются параметры действия, а значениями набор параметров, таких как
     * обязательность, макс. длина и другие.
     *
     * @param actionName имя действия, соответсвует значению из колонки ID таблицы public.actions
     * @return строка в формате JSON
     */
    @Override
    public String getActionParams(String actionName) {
        ParamStructure fullParamStructure = new ParamStructure();
        final String sql = "SELECT ap.param_id, p.kias_name, "
                + "ap.is_required, p.descr, pt.descr AS param_type, "
                + "p.minlength, p.maxlength, p.mask "
                + "FROM public.action_params ap "
                + "JOIN public.params p ON ap.param_id = p.id "
                + "JOIN public.paramtypes pt ON p.type_id = pt.id "
                + "JOIN public.actions ac ON ac.id = ap.action_id "
                + "WHERE ap.action_id = ? " + isShowAllQueryWithAnd()
                + " AND NOT exists(SELECT udp.param_id FROM public.user_defined_params udp WHERE udp.action_id = ? AND udp.param_id = ap.param_id) "
                + "ORDER BY ap.param_order";
        try (Connection conn = DbUtils.getConnection();
                PreparedStatement ps = conn.prepareStatement(sql)) {
            ps.setString(1, actionName);
            ps.setString(2, actionName);
            try (ResultSet rs = ps.executeQuery()) {
                while (rs.next()) {
                    String paramPublicName = rs.getString("param_id");
                    String paramKiasName = rs.getString("kias_name");
                    String descr = rs.getString("descr");
                    String paramType = rs.getString("param_type");
                    boolean required = rs.getBoolean("is_required");
                    int minlength = rs.getInt("minlength");
                    int maxlength = rs.getInt("maxlength");
                    String mask = rs.getString("mask");
                    ActionParam param = new ActionParam(paramPublicName, paramKiasName, descr, paramType, required,
                            minlength, maxlength, mask);
                    param.setValue(getValueAsJson(descr, paramType, required, minlength, maxlength, mask));
                    param.setConfirmed(true);

                    // TODO sql должен возвращать информацию о всех массивах, причем непосредственно перед первым
                    // параметром из этого массива (т.е. attributes-attribute непосредственно перед
                    // attributes-attribute-id).
                    //
                    // Обработка массива здесь: fullParamStructure.addList("attributes-attribute");
                    //
                    if ("attributes-attribute-id".equalsIgnoreCase(paramPublicName)) {
                        fullParamStructure.addList("attributes-attribute", 2);
                    }

                    fullParamStructure.add(param);
                }
                // закончили бежать по полям
            }
        } catch (SQLException e) {
            Log.getLogger("db").error(e.getLocalizedMessage());
        }
        return fullParamStructure.getValuesAsXml(false);
    }

    /**
     * Формирует из переданных параметров плоский JSON.
     *
     * @param descr описание параметра
     * @param paramType тип параметра
     * @param required обязательность параметра
     * @param minlength минимальная длина
     * @param maxlength максимальная длина
     * @param mask маска параметра
     * @return плоский JSON
     */
    private JSONObject getValueAsJson(String descr, String paramType, boolean required, int minlength, int maxlength,
            String mask) {
        Map<String, Object> result = new TreeMap<>();
        result.put("descr", descr);
        result.put("paramType", paramType);
        result.put("required", required);
        if (minlength != 0) {
            result.put("minlength", String.valueOf(minlength));
        }
        if (maxlength != 0) {
            result.put("maxlength", String.valueOf(maxlength));
        }
        if (mask != null) {
            result.put("mask", mask);
        }
        return new JSONObject(result);
    }

    /**
     * @param actionName имя действия, соответсвует значению из колонки ID таблицы public.actions
     * @return пример успешного ответа
     */
    @Override
    public String getSuccessResponse(String actionName) {
        final String sql = "SELECT ad.success_resp FROM public.actions_data ad JOIN public.actions ac ON ac.id = ad.id WHERE ad.id = ? "
                + isShowAllQueryWithAnd();
        return selectFromDb(sql, actionName);
    }

    /**
     * @param actionName имя действия, соответсвует значению из колонки ID таблицы public.actions
     * @return пример ошибочного ответа
     */
    @Override
    public String getErrorResponse(String actionName) {
        final String sql = "SELECT ad.error_resp FROM public.actions_data ad JOIN public.actions ac ON ac.id = ad.id WHERE ad.id = ? "
                + isShowAllQueryWithAnd();
        return selectFromDb(sql, actionName);
    }

    /**
     * Вспомогательный метод для получения примеров ответов сервера КИАС.
     *
     * @param sql запрос к БД в виде строки
     * @param actionName имя действия
     * @return ответ базу данных в виде строки
     */
    private String selectFromDb(String sql, String actionName) {
        String result = "";
        try (Connection conn = DbUtils.getConnection();
                PreparedStatement ps = conn.prepareStatement(sql)) {
            ps.setString(1, actionName);
            try (ResultSet resultSet = ps.executeQuery()) {
                if (resultSet.next()) {
                    result = resultSet.getString(1);
                }
            }
        } catch (SQLException e) {
            Log.getLogger("db").error(e.getLocalizedMessage());
        }
        return convertToXml(result);
    }

    /**
     * Конвертируем результаты полученные из БД в виде JSON в XML, полученные в виде XML пропускаем как есть.
     *
     * @param result результат полученный из БД
     * @return результат в виде XML строки
     */
    private String convertToXml(String result) {
        result = (result == null) ? "" : result;
        try {
            JSONObject jsonObject = new JSONObject(result);
            return XML.toString(jsonObject);
        } catch (JSONException e) {
            return result;
        }
    }

    /**
     * Получаем из файла настроек приложения признак выводить ли всю документацию.
     *
     * @return пустую строку если выводить всю иначе дополнительное условие для запроса
     */
    private String isShowAllQuery() {
        boolean isShowAll = Boolean.parseBoolean(Utils.APP_PROPERTIES.getProperty("documentation.show.all"));
        return isShowAll ? "" : "document = TRUE";
    }

    /**
     * Если дополнительное условие не пустое, добавляем {@code AND} перед ним.
     *
     * @return дополненное условие или пустую строку
     */
    private String isShowAllQueryWithAnd() {
        String showAll = isShowAllQuery();
        return Utils.notEmpty(showAll) ? ("AND " + showAll) : showAll;
    }
}
