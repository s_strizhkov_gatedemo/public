package ru.verna.alfabank;

/**
 * Класс, представляющий ответ Альфа-банка.
 */
class AlfaBankResponse {
    private int code;
    private String msg;

    /**
     * Полный конструктор.
     *
     * @param code код из ответа
     * @param msg  сообщение из ответа
     */
    AlfaBankResponse(int code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    /**
     * Возвращает код текущего объекта.
     *
     * @return код текущего объекта
     */
    public int getCode() {
        return code;
    }

    /**
     * Возвращает сообщение текущего объекта.
     *
     * @return сообщение текущего объекта
     */
    public String getMsg() {
        return msg;
    }

    @Override
    public String toString() {
        return "AlfaBankResponse{"
                + "code=" + code
                + ", msg='" + msg + '\''
                + '}';
    }
}
