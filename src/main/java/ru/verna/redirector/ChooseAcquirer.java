package ru.verna.redirector;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.URLDecoder;
import java.util.HashMap;
import java.util.Map;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONObject;
import org.slf4j.Logger;
import ru.verna.apublic.Util;
import ru.verna.commons.Utils;
import ru.verna.commons.log.Log;

/**
 * Выбор эквайрера. Сейчас только Альфа-банк. Класс возвращает url страницы оплаты котировки с номером calc_id.
 */
@WebServlet(name = "ChooseAcquirer", urlPatterns = "/chooseacquirer")
public class ChooseAcquirer extends HttpServlet {

    private static final long serialVersionUID = 1L;

    /**
     * Выбор эквайрера. Сейчас только Альфа-банк. Метод возвращает ссылку на страницу оплаты в Альфа-банке.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws IOException {
        Logger mainLogger = Log.getLogger("main");

        boolean linkIsLegal = false;
        String retrievedSecret = request.getParameter("time");
        if (Utils.notEmpty(retrievedSecret)) {
            String salt = retrievedSecret.substring(0, 4);
            String secretPassword = "sEcReTpAsSwOrD_$#@!";
            String newMD5 = Utils.md5(salt + secretPassword);
            if (retrievedSecret.equals(salt + newMD5)) {
                linkIsLegal = true;
            }
        }

        if (linkIsLegal) {
            // запрашиваем url Альфа-банка в jhermes'е
            String action = "getAlfabankUrl";

            String calcId = request.getParameter("calc_id");
            String partnerSuccessURL = request.getParameter("partner_success_url");
            String partnerFailureURL = request.getParameter("partner_failure_url");
            String partnerCallbackURL = request.getParameter("callback_url");
            String partnerId = request.getParameter("partner_id");

            Map<String, String> parameters = new HashMap<>(6);
            parameters.put("calc_id", calcId);
            parameters.put("partner_success_url", partnerSuccessURL);
            parameters.put("partner_failure_url", partnerFailureURL);
            parameters.put("partner_callback_url", partnerCallbackURL);
            parameters.put("partner_id", partnerId);
            parameters.put("f", "json");

            try {
                String gateResponse = Util.callAction(request, action, parameters, null);

                JSONObject responseObject = new JSONObject(gateResponse);
                JSONObject data = responseObject.getJSONObject("data");
                if (data.has("result")) {
                    JSONObject result = data.getJSONObject("result");
                    String encodedAcquirerUrl = result.getString("url");
                    String decodedAcquirerUrl = URLDecoder.decode(encodedAcquirerUrl, "UTF-8");

                    response.setStatus(302);
                    response.setHeader("Location", decodedAcquirerUrl);

                } else if (data.has("error")) {
                    response.setContentType("application/json;charset=UTF-8");
                    mainLogger.debug("ответ: {}", gateResponse);
                    response.getWriter().print(gateResponse);
                }
            } catch (Exception e) {
                mainLogger.error(null, e);
            }

        } else {
            // кто-то шлет левый time, ссылка не наша
            response.setContentType("text/html;charset=UTF-8");
            try (PrintWriter out = response.getWriter()) {
                out.println("<!DOCTYPE html>");
                out.println("<html><head><title>Ошибка</title></head>");
                out.println("<body><p>");
                out.println("Ошибочная ссылка");
                out.println("</p></body></html>");
            }
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the
    // code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }
    // </editor-fold>

}
