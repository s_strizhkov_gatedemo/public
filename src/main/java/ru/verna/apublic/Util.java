package ru.verna.apublic;

import org.apache.http.client.methods.HttpGet;
import org.slf4j.Logger;
import ru.verna.commons.log.Log;
import ru.verna.commons.http.CommonHttpClient;
import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

/**
 *
 */
public final class Util {

    /**
     * Закрытый конструктор.
     */
    private Util() {
    }

    /**
     * Вызывает заданное действие шлюза.
     * <br>
     * Результат будет выдан в формате по умолчанию (json).
     *
     * @param request запрос
     * @param action  действие
     * @param params  параметры вызова. Может быть null.
     * @param headers заголовки. Может быть null.
     * @return строку отклика действия
     * @throws Exception в случае проблем
     */
    public static String callAction(HttpServletRequest request, String action, Map<String, String> params, Map<String, String> headers)
            throws Exception {
        Logger webLogger = Log.getLogger("web");

        String scheme = request.getScheme();
        String serverName = request.getServerName();
        int serverPort = request.getServerPort();
        // TODO некрасивые преобразования, переделать. Возможно, использовать серверо-зависимую часть БД (как схема kias) или таблицу config.
        if ("gate-as-test".equalsIgnoreCase(serverName) || "gate-as-prod".equalsIgnoreCase(serverName)) {
            serverName = serverName + ".exprem.dom";
        }
        String url;
        if (scheme.equalsIgnoreCase("https") && serverPort == 443) {
            url = scheme + "://" + serverName + "/gate/" + action;
        } else {
            url = scheme + "://" + serverName + ":" + serverPort + "/gate/" + action;
        }
        Map<String, String> fullHeaders = new HashMap<>();
        fullHeaders.put("Authorization", "Bearer tokenofpublicuser");
        if (headers != null) {
            fullHeaders.putAll(headers);
        }
        long txNumber = Math.round(Math.random() * 1000000000);
        webLogger.info("tx {} REQUEST: {}", txNumber, url);
        String gateResponse = CommonHttpClient.send(HttpGet.METHOD_NAME, url, params, fullHeaders);
        webLogger.info("tx {} RESPONSE: {}", txNumber, gateResponse);
        return gateResponse;
    }
}
