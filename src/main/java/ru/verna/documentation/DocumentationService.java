package ru.verna.documentation;

/**
 * Интерфейс получения данных по документации из БД.
 */
interface DocumentationService {

    /**
     * @return список действий из таблицы public.actions
     */
    String getActionList();

    /**
     * @param actionName имя действия, соответсвует значению из колонки ID таблицы public.actions
     * @return параметры действия
     */
    String getActionParams(String actionName);

    /**
     * @param actionName имя действия, соответсвует значению из колонки ID таблицы public.actions
     * @return пример успешного ответа
     */
    String getSuccessResponse(String actionName);

    /**
     * @param actionName имя действия, соответсвует значению из колонки ID таблицы public.actions
     * @return пример ошибочного ответа
     */
    String getErrorResponse(String actionName);
}
