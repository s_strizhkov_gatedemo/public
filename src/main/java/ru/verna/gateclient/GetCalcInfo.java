package ru.verna.gateclient;

import ru.verna.apublic.Util;
import ru.verna.commons.log.Log;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

/**
 * Возвращает информацию о состоянии котировки (полиса) по номеру котировки.
 */
@WebServlet(name = "GetCalcInfo", urlPatterns = { "/getcalcinfo" })
public class GetCalcInfo extends HttpServlet {

    static final long serialVersionUID = 1L;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        // TODO брать content type из отклика jhermes
        response.setContentType("application/json;charset=UTF-8");

        // String mock = request.getParameter("mock");
        // мок отключен программно (if false)
        if (false /* && "1".equalsIgnoreCase(mock) || "true".equalsIgnoreCase(mock) */) {
            // мокаем ответ
            try (PrintWriter out = response.getWriter()) {
                out.print("{\n"
                        + " \"data\": {\n"
                        + "     \"result\": {\n"
                        + "         \"product\": {\n"
                        + "             \"product_isn\": 1280241,\n"
                        + "             \"name\": \"Федерация танцевального спорта\"\n"
                        + "         },\n"
                        + "         \"documents\": {\n"
                        + "             \"document\": [\n"
                        + "                 {\n"
                        + "                     \"path\": \"\\\\ST03.EXPREM.DOM\\KIASFOTO\\C58574\\151312186.pdf\",\n"
                        + "                     \"filename\": \"Выписка_из_ЕГРИП_ИНН_132611151288_11.05.2018.pdf\",\n"
                        + "                     \"document_isn\": \"Идентификатор документа\",\n"
                        + "                     \"description\": { },\n"
                        + "                     \"type\": \"Анкета идентификации контрагента - ИП (версия 1.2.0 от 12.2017)\"\n"
                        + "                 }\n"
                        + "                 ,\n"
                        + "                 {\n"
                        + "                     \"path\": \"\\\\ST03.EXPREM.DOM\\KIASFOTO\\C58574\\151312186.pdf\",\n"
                        + "                     \"filename\": \"Выписка_из_ЕГРИП_ИНН_132611151288_11.05.2018.pdf\",\n"
                        + "                     \"document_isn\": \"Идентификатор документа\",\n"
                        + "                     \"description\": { },\n"
                        + "                     \"type\": \"Анкета идентификации контрагента - ИП (версия 1.2.0 от 12.2017)\"\n"
                        + "                 }\n"
                        + "                 ,\n"
                        + "                 {\n"
                        + "                     \"path\": \"\\\\ST03.EXPREM.DOM\\KIASFOTO\\C58574\\151312186.pdf\",\n"
                        + "                     \"filename\": \"Выписка_из_ЕГРИП_ИНН_132611151288_11.05.2018.pdf\",\n"
                        + "                     \"document_isn\": \"Идентификатор документа\",\n"
                        + "                     \"description\": { },\n"
                        + "                     \"type\": \"Анкета идентификации контрагента - ИП (версия 1.2.0 от 12.2017)\"\n"
                        + "                 }\n"
                        + "             ]\n"
                        + "         },\n"
                        + "         \"insurer\": {\n"
                        + "             \"patronymic\": \"Петрович\",\n"
                        + "             \"phone\": \"+7-900-123-45-67\",\n"
                        + "             \"surname\": \"Петров\",\n"
                        + "             \"name\": \"Петр\",\n"
                        + "             \"email\": \"petrov@mail.ru\"\n"
                        + "         },\n"
                        + "         \"calc\": {\n"
                        + "             \"calc_id\": \"7992-32O7SF9\",\n"
                        // + " \"calc_id\": \"6988-DDV4WCG\",\n"
                        + "             \"status\": \"Оформление\"\n"
                        + "         },\n"
                        + "         \"policy\": {\n"
                        + "             \"end_date\": \"31.02.2019\",\n"
                        + "             \"policy_id\": \"200008/18/00003/9302001\",\n"
                        + "             \"begin_date\": \"31.02.2018\",\n"
                        + "             \"status\": \"Выпущен\"\n"
                        + "         }\n"
                        + "     }\n"
                        + " }\n"
                        + "}");
            }

        } else {
            Map<String, String> params = new HashMap<>(1);
            String calcId = request.getParameter("calc_id");
            params.put("calc_id", calcId);

            try {
                String gateResponse = Util.callAction(request, "getCalcInfoExtended", params, null);
                try (PrintWriter out = response.getWriter()) {
                    out.print(gateResponse);
                }

            } catch (Exception e) {
                Log.getLogger("web").error(null, e);
            }
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the
    // code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }
    // </editor-fold>

}
