package ru.verna.documentation;

import java.io.IOException;
import javax.inject.Inject;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * РЕСТ сервлет, возвращающий непосредственно данные из БД для документации.
 */
@WebServlet(name = "DocumentationRestServlet", urlPatterns = "/documentation/rest/*")
public class DocumentationRestServlet extends HttpServlet {

    private static final long serialVersionUID = 1L;

    @Inject
    private DocumentationService documentationService;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        String successParam = "success";
        String errorParam = "error";
        String dbResponse;
        if ("/list".equals(req.getPathInfo())) {
            dbResponse = documentationService.getActionList();
        } else if (req.getPathInfo().endsWith(successParam)) {
            dbResponse = documentationService.getSuccessResponse(getActionName(req.getPathInfo()));
        } else if (req.getPathInfo().endsWith(errorParam)) {
            dbResponse = documentationService.getErrorResponse(getActionName(req.getPathInfo()));
        } else {
            dbResponse = documentationService.getActionParams(clearFirstSlash(req.getPathInfo()));
        }
        resp.setContentType("application/json;charset=UTF-8");
        resp.setHeader("Access-Control-Allow-Origin", req.getHeader("Origin"));
        resp.getWriter().write(dbResponse);
    }

    /**
     * Отрезает от строки запроса последнюю часть, не содержащую имя действия.
     *
     * @param pathInfo {@link HttpServletRequest#getPathInfo()} в виде строки
     * @return имя действия
     */
    private String getActionName(String pathInfo) {
        return clearFirstSlash(pathInfo).substring(0, pathInfo.lastIndexOf("/") - 1);
    }

    /**
     * Если переданная строка начинается со слэша, то удаляет его.
     *
     * @param pathInfo {@link HttpServletRequest#getPathInfo()} в виде строки
     * @return строка без слэша
     */
    private String clearFirstSlash(String pathInfo) {
        return pathInfo.startsWith("/") ? pathInfo.replaceFirst("/", "") : pathInfo;
    }
}
