// 
var theTimer;
/**
 * 
 */
function verify_policy() {
    var url = 'getCalcInfo';

    $.post(url, {calc_id: calc_id /*, mock: 1*/}, function (response) {
        console.log(response);
        var data = response.data;
        if (data.result) {//if (typeof data.result !== 'undefined')
            // пришел ответ
            var dataResult = data.result;
            if (dataResult.policy && dataResult.policy.policy_id) {
                // получили номер договора
                // отключаем опрос сервера по таймеру
                clearTimeout(theTimer);

                var res = $('#payment_policy_result');
                var insurer = dataResult.insurer;
                var insurerFio = insurer.name + ' ' + insurer.patronymic + ' ' + insurer.surname;

                $('#insurer', res).text(insurerFio);
                $('#email', res).text(insurer.email);
                $('#policy_id', res).text(dataResult.policy.policy_id);
                $('#begin_date', res).text(dataResult.policy.begin_date);
                $('#end_date', res).text(dataResult.policy.end_date); 
                
                var policy_data_container = $('#policy_data_container');
                var needDescr = true;
                var doc = dataResult.documents.document;
                var htmlString = "<p><span>" + (needDescr ? "Документы:" : "&nbsp;") + "</span><a href='download?document_id=" + doc.document_isn + "'>" + doc.filename + "</a></p>";
                policy_data_container.append(htmlString);
                needDescr = false;
                /*var needDescr = true;
                for (var doc in dataResult.documents.document) {
                    console.log(doc);
                    var htmlString = "<p><span>" + (needDescr ? "Документы:" : "&nbsp;") + "</span><b class='doclink'>" + doc.filename + "</b></p>";
                    policy_data_container.append(htmlString);
                    needDescr = false;
                }*/
                res.addClass('active');
                $('#status_wait').remove();
            } else {
                var calcStatus = $('#calc_status');
                calcStatus.text("Статус: " + dataResult.calc_status + ". Получение данных полиса...");
            }

        } else if (data.error) {
            // пришла ошибка
            // отключаем опрос сервера по таймеру
            clearTimeout(theTimer);

            $('#status_wait').remove();
            $('#policy_error .error-text').text(data.error.text);
            $('#policy_error').show();
        }
        /*if (data.result && data.policy.policy_id && !data.hasOwnProperty('error')) {
            // отключаем опрос сервера по таймеру
            clearTimeout(theTimer);

            var res = $('#payment_policy_result');
            var dataResult = data.result;
            var insurer = dataResult.insurer;
            var insurerFio = insurer.name + ' ' + insurer.patronymic + ' ' + insurer.surname;

            $('#insurer', res).text(insurerFio);
            $('#email', res).text(insurer.email);
            $('#policy_id', res).text(dataResult.policy.policy_id);
            $('#begin_date', res).text(dataResult.policy.begin_date);
            $('#end_date', res).text(dataResult.policy.end_date);
            res.addClass('active');
            $('#status_wait').remove();
        } else {
            if (data.error.row['attr_name'] == 'policy_id') {
                verify_policy();
            } else {
                $('#status_wait').remove();
                $('#error_info .error-text').text(data.error.row['text']);
                $('#error_info').show();
            }
        }*/
    }, 'json');
}

$(function () {
    if (payment_success) {
        // запускаем опрос сервера по таймеру
        theTimer = setInterval(verify_policy, 7000);
        //verify_policy();
    }
});
