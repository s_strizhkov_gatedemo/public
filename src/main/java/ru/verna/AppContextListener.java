package ru.verna;

import ru.verna.commons.Utils;
import ru.verna.commons.log.Log;
import ru.verna.commons.log.ThreadUtil;

import javax.servlet.ServletRequestEvent;
import javax.servlet.ServletRequestListener;
import javax.servlet.http.HttpServletRequest;

/**
 * HTTP request lifecycle listener.
 * Манипулируем в контексте запроса ThreadLocal переменными.
 */
public class AppContextListener implements ServletRequestListener {

    @Override
    public void requestInitialized(ServletRequestEvent sre) {
        String txNumber = String.valueOf(Utils.getRandomLong());
        String callContext = ((HttpServletRequest) sre.getServletRequest()).getHeader(Log.CALL_CONTEXT_HEADER);
        ThreadUtil.setThreadVariable(Log.TX_NUMBER, txNumber);
        ThreadUtil.setThreadVariable(Log.CALL_CONTEXT, callContext);
    }

    @Override
    public void requestDestroyed(ServletRequestEvent sre) {
        ThreadUtil.destroy();
    }
}
