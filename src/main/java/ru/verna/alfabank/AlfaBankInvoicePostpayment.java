package ru.verna.alfabank;

import com.jayway.jsonpath.JsonPath;
import org.apache.http.client.methods.HttpGet;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import ru.verna.apublic.Util;
import ru.verna.commons.DbUtils;
import ru.verna.commons.Utils;
import ru.verna.commons.http.CommonHttpClient;
import ru.verna.commons.log.Log;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Сюда перенаправляется запрос с корпоративного сайта после оплаты счета в Альфа-банке.
 */
@WebServlet(name = "AlfaBankInvoicePostpayment", urlPatterns = {"/abi_postpayment"})
public class AlfaBankInvoicePostpayment extends HttpServlet {

    private static final long serialVersionUID = 1L;
    /**
     * Человекочитаемое описание совершаемого действия.
     */
    private static final String OPERATION_HUMAN_DESCR = "Оплата счета";

    /**
     * Человекочитаемое описание совершаемого действия в родительном падеже.
     */
    private static final String OPERATION_HUMAN_DESCR_ROD = "оплаты счета";

    /**
     * Человекочитаемое описание оплачиваемой сущности в родительном падеже.
     */
    private static final String PAID_ENTITY_DESCR_ROD = "счета";

    /**
     * Имя параметра для приема и отправки идентификатора оплачиваемой сущности (счета).
     */
    private static final String PAID_ENTITY_PARAM_NAME = "invoice_isn";

    /**
     * Сюда Альфа-банк перенаправит пользователя после успешной (?success=1) или неуспешной (?success=0) оплаты.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws IOException if an I/O error occurs
     */
    private void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws IOException {
        Logger mainLogger = Log.getLogger("main");
        // Альфа-банк присылает свой orderId
        String orderId = request.getParameter("orderId");

        // в БД в таблице config настройка alfabank_mode задает, к какому серверу Альфа-банка подключаться, к боевому
        // или тестовому
        Map<String, String> alfabankModeConfig = DbUtils.getConfigs("alfabank_mode");
        String alfabankMode = alfabankModeConfig.get("alfabank_mode");

        Map<String, String> alfabankConfigs = DbUtils.getConfigs(alfabankMode);
        // запрашиваем доп.параметры заказа
        String alfaServer = alfabankConfigs.get("baseurl") + alfabankConfigs.get("getOrderStatus");
        Map<String, String> alfaParams = new HashMap<>();
        alfaParams.put("userName", alfabankConfigs.get("username"));
        alfaParams.put("password", alfabankConfigs.get("password"));
        alfaParams.put("orderId", orderId);
        alfaParams.put("language", "ru");

        try {
            String acquirerResponse = CommonHttpClient.send(HttpGet.METHOD_NAME, alfaServer, alfaParams, null);
            JSONObject acquirerResponseAsJSON = new JSONObject(acquirerResponse);

            // оплачиваемая сущность (invoiceIsn)
            String paidEntityId = "";
            int bankActionCode = acquirerResponseAsJSON.getInt("actionCode");
            String bankActionDescr = acquirerResponseAsJSON.getString("actionCodeDescription");
            // сумма платежа в копейках
            long amount = acquirerResponseAsJSON.getLong("amount");
            boolean success = (bankActionCode == 0);

            JSONArray savedOrderParams = acquirerResponseAsJSON.getJSONArray("merchantOrderParams");
            String partnerSuccessURL = "";
            String partnerFailureURL = "";
            String partnerCallbackURL = "";
            String postbankURL = "";
            for (int i = 0, len = savedOrderParams.length(); i < len; i++) {
                JSONObject orderParam = savedOrderParams.getJSONObject(i);
                String paramName = orderParam.getString("name");
                String paramValue = orderParam.getString("value");
                switch (paramName) {
                    case "postbank_url":
                        postbankURL = paramValue;
                        break;
                    case "partner_success_url":
                        partnerSuccessURL = paramValue;
                        break;
                    case "partner_failure_url":
                        partnerFailureURL = paramValue;
                        break;
                    case "partner_callback_url":
                        partnerCallbackURL = paramValue;
                        break;
                    case PAID_ENTITY_PARAM_NAME:
                        paidEntityId = paramValue;
                        break;
                    default:

                }
            }

            Map<String, String> params = new HashMap<>(1);
            params.put("isn", paidEntityId);

            boolean saveToRSA = getPaymentDataAndPrintLog(request, params, success);

            // если оплата успешна, отметить счет как оплаченный
            if (success) {
                try {
                    // результат будет в формате по умолчанию (json)
                    String postPaymentActionResponse;
                    if (saveToRSA) {
                        postPaymentActionResponse = Util.callAction(request, "products/osago/createagr", params, null);
                    } else {
                        postPaymentActionResponse = Util.callAction(request, "invoiceSetPaid", params, null);
                    }
                    JSONObject postPaymentResult = new JSONObject(postPaymentActionResponse);
                    if (!postPaymentResult.has("data")) {
                        success = false;
                        bankActionDescr = postPaymentActionResponse;
                    }
                    JSONObject dataJSON = postPaymentResult.getJSONObject("data");

                    if (dataJSON.has("error") && isReverseInvoice(alfabankConfigs)) {
                        String reverseOrder = AlfaBankUtils.reverseOrder(paidEntityId, alfabankConfigs, alfaParams);
                        AlfaBankResponse reverseResponse = AlfaBankUtils.wrapReverseOrderResponse(reverseOrder);
                        success = false;
                        long ticket = Utils.getRandomLong(4);
                        mainLogger.error("{}, инцидент {}: {}", OPERATION_HUMAN_DESCR, ticket,
                                postPaymentActionResponse);

                        String reverseSuccessMsg = "\nМы отменили оплату " + PAID_ENTITY_DESCR_ROD
                                + ", ожидайте возврат денежных средств в ближайшее время.";
                        String reverseFailureMsg = "\nНами был отправлен запрос на отмену оплаты "
                                + PAID_ENTITY_DESCR_ROD + ". "
                                + "\nВо время отмены оплаты " + PAID_ENTITY_DESCR_ROD + " произошла ошибка. "
                                + "\nМы занимаемся данной проблемой и свяжемся с вами в ближайшее время.";
                        bankActionDescr = "Во время " + OPERATION_HUMAN_DESCR_ROD + " произошла ошибка."
                                + ((reverseResponse.getCode() == 0) ? reverseSuccessMsg : reverseFailureMsg);

                        // отправляем сообщения для реагирования на ситуацию
                        AlfaBankUtils.sendWarningMail(request, bankActionDescr, postPaymentActionResponse,
                                reverseResponse, ticket);
                    }
                } catch (Exception e) {
                    success = false;
                    long ticket = Utils.getRandomLong(4);
                    mainLogger.error(OPERATION_HUMAN_DESCR + ", инцидент {}: {}: {}", ticket,
                            e.getClass().getSimpleName(),
                            e.getMessage());
                    mainLogger.error(null, e);
                    bankActionDescr = "Во время " + OPERATION_HUMAN_DESCR_ROD
                            + " произошла ошибка. Пожалуйста, сообщите администратору системы номер инцидента: "
                            + ticket;
                }

                // в случае успеха отправляем извещение партнеру на partnerCallbackURL
                if (Utils.notEmpty(partnerCallbackURL)) {
                    AlfaBankUtils.noticePartner(partnerCallbackURL, amount);
                } else {
                    mainLogger.trace("Отсутствует адрес для коллбэка партнеру, коллбэк не отправляем");
                }
            }

            // редирект на постбанковскую страницу
            Map<String, String> postbankParams = new LinkedHashMap<>();
            postbankParams.put("success", success ? "1" : "0");
            postbankParams.put(PAID_ENTITY_PARAM_NAME, paidEntityId);
            postbankParams.put("partner_success_url", partnerSuccessURL);
            postbankParams.put("partner_failure_url", partnerFailureURL);
            postbankParams.put("bankActionDescr", bankActionDescr);

            String joinedParameters = Utils.joinParameters(postbankParams);
            String delim = postbankURL.contains("?") ? "&" : "?";
            String redirectUrl = postbankURL + delim + joinedParameters;

            Log.getLogger("web").trace("redirect to postbank page: {}", redirectUrl);
            response.setStatus(HttpServletResponse.SC_MOVED_TEMPORARILY);
            response.setHeader("Location", redirectUrl);

        } catch (Exception e) {
            mainLogger.error(null, e);
            response.setContentType("text/html;charset=UTF-8");
            try (PrintWriter out = response.getWriter()) {
                e.printStackTrace(out);
            }
        }
    }

    /**
     * Получаем информацию о продукте и выводим данные в лог. Возвращаем признак является ли счет на оплату по ОСАГО и
     * необходимость создания договора.
     *
     * @param request входящий запрос
     * @param params параметры для вызова действия
     * @param success признак успешной оплаты
     * @return {@code true} если признак счета "saveToRSA" равен 1, что свидетельствует о необходимости дальнейших
     *         действий по сохранению договора в РСА, иначе {@code false}
     */
    private boolean getPaymentDataAndPrintLog(HttpServletRequest request, Map<String, String> params, boolean success) {
        Logger paymentLogger = Log.getLogger("payment");
        String saveToRSA = "";
        try {
            String paymentData = Util.callAction(request, "getInvoiceInfo", params, null);
            String orderNumber = JsonPath.read(paymentData, "$.data.result.number");
            String purpose = JsonPath.read(paymentData, "$.data.result.purpose");
            String calcSum = JsonPath.read(paymentData, "$.data.result.amount");
            saveToRSA = JsonPath.read(paymentData, "$.data.result.saveToRSA");
            paymentLogger.info(
                    (success ? "Успешная" : "Неуспешная") + " оплата: счет: [{}], orderId:[{}], сумма: summ[{}]",
                    orderNumber, purpose, calcSum);
        } catch (Exception e) {
            paymentLogger.error("Ошибка при попытке получить данные счета для логирования", e);
        }
        return "1".equals(saveToRSA);
    }

    /**
     * Возвращает признак, надо ли отменять оплату в Альфабанке.
     *
     * @param alfabankConfigs параметры Альфабанка
     * @return {@code true} если надо отменять оплату иначе {@code false}
     */
    private boolean isReverseInvoice(Map<String, String> alfabankConfigs) {
        return Boolean.parseBoolean(alfabankConfigs.get("isReverseInvoice"));
    }

    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }
}
