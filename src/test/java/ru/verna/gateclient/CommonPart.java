package ru.verna.gateclient;

import io.restassured.RestAssured;
import io.restassured.builder.RequestSpecBuilder;
import ru.verna.commons.Utils;
import ru.verna.commons.log.Log;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import static io.restassured.config.SSLConfig.sslConfig;

/**
 * Настройки тестов.
 */
public class CommonPart {

    /**
     * Токен пользователя www.
     */
    public static final String TOKEN_USER_WWW = "9e0c10d6a4e852f307ba4df305349f5becb7fd03dc4ff515d8839918865a69abd6341f9543d8eddd7d8d05e7d49b68db1c9f5f19a020b56f1bf6d1d4198f6760";
    /**
     * Токен пользователя public.
     */
    public static final String TOKEN_USER_PUBLIC = "tokenofpublicuser";
    private static final String RESOURCES_FILE_PATH = "test.properties";
    private static Properties testProperties = new Properties();

    static {
        try (InputStream resource = CommonPart.class.getClassLoader().getResourceAsStream(RESOURCES_FILE_PATH)) {
            CommonPart.testProperties.load(resource);
        } catch (IOException e) {
            Log.getLogger("test").error("Cannot read test resources = {}", e.getMessage());
        }
    }

    /**
     * Закрытый конструктор.
     */
    private CommonPart() {
    }

    public static void setServer() {
        String testableAppserverScheme = System.getProperty("testable_appserver_scheme", "http");
        String testableAppserverHost = System.getProperty("testable_appserver_host", "localhost");
        Integer testableAppserverPort = Integer.parseInt(System.getProperty("testable_appserver_port", "8080"));
        String testableAppserverContextRoot = System.getProperty("testable_appserver_contextroot", "public");

        /**
         * Хост тестируемого сервера приложений.
         */
        RestAssured.baseURI = testableAppserverScheme + "://" + testableAppserverHost;

        /**
         * Порт тестируемого сервера приложений.
         */
        RestAssured.port = testableAppserverPort;

        /**
         * Контекст приложения (путь) на тестируемом сервере приложений.
         */
        RestAssured.basePath = "/" + testableAppserverContextRoot;

        /**
         * Добавление поддержки SSL в конфигурацию
         */
        RestAssured.config = RestAssured.config().sslConfig(sslConfig().allowAllHostnames().relaxedHTTPSValidation());
    }

    public static String getProperty(String key) {
        return testProperties.getProperty(key);
    }

    public static RequestSpecBuilder getRequestSpecBuilder(String userToken) {
        RequestSpecBuilder builder = new RequestSpecBuilder();
        builder.addHeader(Log.CALL_CONTEXT_HEADER, Log.TEST_CALL_CONTEXT);
        if (Utils.notEmpty(userToken)) {
            builder.addHeader("authorization", "Bearer " + userToken);
        }
        return builder;
    }
}
