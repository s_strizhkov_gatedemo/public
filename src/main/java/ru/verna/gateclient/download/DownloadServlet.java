package ru.verna.gateclient.download;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.lang3.SystemUtils;
import org.json.JSONObject;
import org.slf4j.Logger;
import ru.verna.apublic.Util;
import ru.verna.commons.Utils;
import ru.verna.commons.log.Log;

/**
 * Выдает клиенту содержимое файла.
 */
@WebServlet(name = "DownloadServlet", urlPatterns = "/download")
public class DownloadServlet extends HttpServlet {

    private static final long serialVersionUID = 1L;

    /**
     * @param request  servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException      if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws IOException {
        Logger mainLogger = Log.getLogger("main");

        Map<String, String> params = new HashMap<>(1);
        String documentId = request.getParameter("document_id");
        params.put("document_id", documentId);

        Enumeration<String> headerNames = request.getHeaderNames();

        Map<String, String> headers = null;
        if (headerNames != null) {
            headers = new HashMap<>();
            String name;
            while (headerNames.hasMoreElements()) {
                name = headerNames.nextElement();
                headers.put(name, request.getHeader(name));
            }
        }

        try {
            String gateResponse = Util.callAction(request, "getDocument", params, headers);
            /*
             * {
             * "data": {
             * "result": {
             * "path": "\\\\ST03.EXPREM.DOM\\KIASFOTO\\C58574\\151312186.pdf",
             * "filename": "Выписка_из_ЕГРИП_ИНН_132611151288_11.05.2018.pdf",
             * "document_isn": 151312186,
             * "description": {
             *
             * },
             * "type": "Анкета идентификации контрагента - ИП (версия 1.2.0 от 12.2017)"
             * }
             * }
             * }
             */

            JSONObject gateResponseAsJSON = new JSONObject(gateResponse);
            if (!gateResponseAsJSON.has("data")) {
                response.setContentType("application/json;charset=UTF-8");
                response.getWriter().write(gateResponse);
            }

            JSONObject dataJSON = gateResponseAsJSON.getJSONObject("data");
            if (dataJSON.has("result")) {
                JSONObject resultJSON = dataJSON.getJSONObject("result");
                String path = resultJSON.getString("path");

                String pathParameter = request.getParameter("path");
                if (pathParameter != null) {
                    path = pathParameter;
                }

                path = path.toLowerCase().replace("\\", "/");
                if (SystemUtils.IS_OS_UNIX) {
                    // на linux-серверах папка с документами (\\st03.exprem.dom\kiasfoto) должна быть примонтирована как
                    // /mnt/st03.exprem.dom/kiasfoto
                    path = "/mnt" + path;
                }
                mainLogger.trace("read filename {}", path);
                // String type = gateResponseAsJSON.getString("type");

                // reads input file from an absolute path
                File downloadFile = new File(path);
                try (FileInputStream inStream = new FileInputStream(downloadFile)) {
                    ServletContext context = getServletContext();

                    // if you want to use a relative path to context root:
                    // String relativePath = getServletContext().getRealPath("");
                    //
                    // gets MIME type of the file
                    String mimeType = context.getMimeType(path);
                    if (mimeType == null) {
                        // set to binary type if MIME mapping not found
                        mimeType = "application/octet-stream";
                    }

                    // modifies response
                    response.setContentType(mimeType);
                    response.setContentLength((int) downloadFile.length());

                    response.setHeader("Cache-Control", "must-revalidate"); // чтобы броузер (и прокси) всегда проверял,
                    // не появилась ли новая версия
                    response.setHeader("Pragma", "no-cache"); // для HTTP 1.0...имеет более низкий приоритет, чем
                    // Cache-Control
                    response.setDateHeader("Expires", System.currentTimeMillis()); // по сути перестраховка для
                    // Cache-Control: must-revalidate
                    response.setHeader("Accept-Ranges", "none"); // подержка докачек пока не реализована

                    // указание браузеру, не встраивать приложение в окно браузера, а открывать отдельно
                    String filename = resultJSON.getString("filename");
                    boolean isFirefox = request.getHeader("user-agent").contains("Firefox");
                    boolean isShow = Boolean.parseBoolean(request.getParameter("show"));
                    if (isShow) {
                        response.setHeader("Content-Disposition", "inline;" + getFileName(filename, isFirefox));
                    } else {
                        response.setHeader("Content-Disposition", "attachment;" + getFileName(filename, isFirefox));
                    }
                    // obtains response's output stream
                    try (OutputStream outStream = response.getOutputStream()) {
                        // CHECKSTYLE:OFF
                        byte[] buffer = new byte[4096];
                        // CHECKSTYLE:ON
                        int bytesRead = -1;
                        while ((bytesRead = inStream.read(buffer)) != -1) {
                            outStream.write(buffer, 0, bytesRead);
                        }
                    }
                } catch (FileNotFoundException fnfe) {
                    response.setContentType("application/json;charset=UTF-8");
                    response.getWriter().write(Utils.getErrorMessage("", "Запрошенный файл не найден"));
                }

            } else if (dataJSON.has("error")) {
                response.setContentType("application/json;charset=UTF-8");
                response.getWriter().write(gateResponse);
            }

        } catch (Exception e) {
            mainLogger.error(null, e);
            response.setContentType("application/json;charset=UTF-8");
            response.getWriter().write(Utils.getErrorMessage("", e.getLocalizedMessage()));
        }

    }

    /**
     * Кодирует оригинальное имя файла и формирует строку заголовка {@code filename}.
     *
     * @param filename  оригинальное имя файла
     * @param isFirefox признак отправлен ли запрос из браузера Firefox
     * @return сформированную строку содержащую закодированое имя файла
     */
    private String getFileName(String filename, boolean isFirefox) {
        String fileNameParam = isFirefox ? "filename*=UTF-8''" : "filename=";
        String fileName = null;
        try {
            fileName = URLEncoder.encode(filename, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            Log.getLogger("main").error("Ошибка при попытке кодировать имя файла [" + filename + "]", e);
        }
        return fileNameParam + "\"" + fileName + "\"";
    }
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the
    // code.">

    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request  servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException      if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request  servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException      if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }
    // </editor-fold>

}
