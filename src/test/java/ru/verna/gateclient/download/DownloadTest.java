package ru.verna.gateclient.download;

import io.restassured.builder.RequestSpecBuilder;
import io.restassured.filter.log.LogDetail;
import io.restassured.specification.RequestSpecification;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import ru.verna.gateclient.CommonPart;
import ru.verna.commons.log.Log;
import javax.servlet.http.HttpServletResponse;
import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.startsWith;

/**
 * Проверка скачивания документа по ссылке /public/download.
 */
public class DownloadTest {

    private static RequestSpecification spec;
    private int documentId;
    private String mimeType;

    @BeforeAll
    public static void setUpClass() {
        CommonPart.setServer();
        RequestSpecBuilder builder = new RequestSpecBuilder();
        builder.addParam("token", CommonPart.TOKEN_USER_PUBLIC);
        builder.addHeader(Log.CALL_CONTEXT_HEADER, Log.TEST_CALL_CONTEXT);
        spec = builder.build();
    }

    /**
     * Контрольные значения, зависящие от тестируемого сервера.
     */
    @BeforeEach
    public void setUp() {
        documentId = Integer.parseInt(CommonPart.getProperty("DownloadTest.documentId"));
        mimeType = CommonPart.getProperty("DownloadTest.mimeType");
    }

    /**
     * получение документа (файла).
     */
    @Test
    public void downloadDoc() {
        given().spec(spec)
                .param("document_id", documentId)
                .log().ifValidationFails(LogDetail.URI)
                .get("download")
                .then()
                .log().ifValidationFails(LogDetail.BODY)
                .statusCode(HttpServletResponse.SC_OK)
                .header("Content-Type", startsWith(mimeType));
    }
}
